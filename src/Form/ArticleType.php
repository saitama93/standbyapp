<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ArticleType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, $this->getConfiguration(
                "Titre",
                "Rentrez un titre"
            ))
            ->add('description', TextareaType::class, $this->getConfiguration(
                "Description",
                "Rentrez une description",
                [
                    'attr' => [
                        'rows' => 5,
                        'class' => "p-3 mt-1 w-full"
                    ]
                ]
            ))
            ->add('type', TextType::class, $this->getConfiguration(
                "Type d'article",
                "Vidéo, article, doc ..."
            ))
            ->add('link', TextType::class, $this->getConfiguration(
                "Lien de l'article",
                "Rentrez un lien"
            ))
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                'label' => 'Catégorie',
                'placeholder' => 'Choisir une catégorie'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
