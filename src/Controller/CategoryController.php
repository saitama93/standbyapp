<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use function PHPUnit\Framework\isEmpty;

class CategoryController extends AbstractController
{
    #[Route('/categorie/liste', name: 'category.index')]
    public function index(CategoryRepository $categoryRepository): Response
    {
        $categories = $categoryRepository->findAll();

        return $this->render('category/index.html.twig', [
            'categories' => $categories
        ]);
    }

    #[Route('/categorie/nouveau', name: 'category.create')]
    #[Security("is_granted('ROLE_USER')")]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $category = new Category();
        $categoryType = $this->createForm(CategoryType::class, $category);
        $categoryType->handleRequest($request);
    
        if($categoryType->isSubmitted() && $categoryType->isValid()){
            $em->persist($category);
            $em->flush();

            $this->addFlash(
                'success',
                "Catégorie créée"
            );

            return $this->redirectToRoute('category.index');
        }

        return $this->render('category/new.html.twig', [
            "categoryType" => $categoryType->createView()
        ]);
    }

    #[Route('/categorie/modification/{id}', name: 'category.edit')]
    #[Security("is_granted('ROLE_USER')")]
    public function edit(Category $category, Request $request, EntityManagerInterface $em): Response
    {
        $categoryType = $this->createForm(CategoryType::class, $category);
        $categoryType->handleRequest($request);
    
        if($categoryType->isSubmitted() && $categoryType->isValid()){
            $em->persist($category);
            $em->flush();

            $this->addFlash(
                'success',
                "Catégorie modifiée"
            );

            return $this->redirectToRoute('category.index');
        }

        return $this->render('category/edit.html.twig', [
            "categoryType" => $categoryType->createView(),
            'category' =>$category
        ]);
    }

    #[Route('/categorie/suppression/{id}', name: 'category.delete')]
    #[Security("is_granted('ROLE_USER')")]
    public function delete(Category $category,EntityManagerInterface $em): Response
    {
        if (isEmpty($category->getArticles())) {
            $this->addFlash(
                'danger',
                "Impossible de supprimer, cette catégorie à des articles assosiés."
            );
    
            return $this->redirectToRoute('category.index');
        }
        $em->remove($category);
        $em->flush();
        
        $this->addFlash(
            'danger',
            "Catégorie supprimée"
        );

        return $this->redirectToRoute('category.index');
    }
}
