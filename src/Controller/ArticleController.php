<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Form\ArticleType;
use App\Repository\UserRepository;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArticleController extends AbstractController
{
    #[Route('/articles', name: 'article.index')]
    public function index(ArticleRepository $articleRepository): Response
    {
        $articles = $articleRepository->orderByDateDESC();
        return $this->render('article/index.html.twig', [
            'articles' => $articles,
        ]);
    }

    #[Route('/article/nouveau', name: 'article.new')]
    #[Security("is_granted('ROLE_USER')")]
    public function create (Request $request, EntityManagerInterface $em, UserRepository $userRepository)
    {
        $article = new Article();
        $articleType = $this->createForm(ArticleType::class, $article);
        $articleType->handleRequest($request);
    
        if($articleType->isSubmitted() && $articleType->isValid()){
            $article->setCreatedAt(new \DateTime('now'))
            ->setAuthor($this->getUser());
            $em->persist($article);
            $em->flush();

            $this->addFlash(
                'success',
                "Article créé"
            );

            return $this->redirectToRoute('article.index');
        }

        return $this->render('article/new.html.twig', [
            'articleType' => $articleType->createView()
        ]);
    }

    #[Route('/article/modification/{id}', name: 'article.edit')]
    #[Security("is_granted('ROLE_USER')")]
    public function edit(Request $request, Article $article, EntityManagerInterface $em)
    {
        $articleType = $this->createForm(ArticleType::class, $article);
        $articleType->handleRequest($request);

        if($articleType->isSubmitted() && $articleType->isValid()){
            $article->setAuthor($this->getUser());
            $em->persist($article);
            $em->flush();

            $this->addFlash(
                'success',
                "Article modifié"
            );

            return $this->redirectToRoute('article.index');
        }
        return $this->render('article/edit.html.twig', [
            'articleType' => $articleType->createView()
        ]);
    }

    #[Route('/article/suppression/{id}', name: 'article.delete')]
    #[Security("is_granted('ROLE_USER')")]
    public function delete(Article $article, EntityManagerInterface $em)
    {
        $em->remove($article);
        $em->flush();
        $this->addFlash(
            'danger',
            "Article supprimé"
        );
        return $this->redirectToRoute('article.index');
    }

    #[Route('/articles/{id}', name: 'category.showArticleByCategory')]
    public function showArticlesByCategory(Category $category, ArticleRepository $articleRepository, CategoryRepository $categoryRepository): Response
    {
        $articles = $articleRepository->findBy(['category' => $category]);
        $categoryName = $categoryRepository->find($category)->getName();
        return $this->render('article/index.html.twig', [
            'articles' => $articles,
            'categoryName' => $categoryName
        ]);
    }
}
