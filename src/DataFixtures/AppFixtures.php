<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Category;
use Faker\Factory;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * Password Hasher
     * @param UserPasswordHasherInterface $userPasswordHasher
     */
    private $userPasswordHasher;
    

    public function __construct( UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher= $userPasswordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create("FR_fr");
        $categories = ["Dev", "Ops", "Security", "Cybersecurity"];
        $types = ["Vidéo", "Article"];
        
        
        for ($j=0; $j < count($categories); $j++) { 
            $category = new Category();
            $category->setName($categories[$j]);
            $manager->persist($category);
            for ($i=0; $i < 3; $i++) { 
                $user = new User();
                $user->setUsername($faker->userName())
                ->setFirstname($faker->firstName())
                ->setLastname($faker->lastName())
                ->setEmail($faker->email())
                ->setRoles(["ROLE_USER"]);
                $hasher = $this->userPasswordHasher->hashPassword($user, "password");
                $user->setPassword($hasher); 
                
                for ($k=0; $k < 3; $k++) { 
                    $article = new Article();
                    $article->setTitle($faker->title())
                    ->setLink("www.youtube.com")
                    ->setDescription($faker->paragraph(5))
                    ->setCategory($category)
                    ->setType($types[rand(0,1)])
                    ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setAuthor($user);
                    $manager->persist($article);
                }
                $manager->persist($user);
            }
        }

        $manager->flush();
    }
}
